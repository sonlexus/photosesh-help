import { SET_CURRENT_USER,UPDATE_STYLE_CODE,DELETE_STYLE_CODE,ADD_STYLE_CODE } from '../actions/types';
import isEmpty from 'lodash/isEmpty';

const initialState = {
  isAuthenticated: false,
  user: {}

};

export default (state = initialState, action = {}) => {
  switch(action.type) {

    case SET_CURRENT_USER:
      return {
        isAuthenticated: !isEmpty(action.user),
        user: action.user
      };

    case ADD_STYLE_CODE:
    {
      let new_state = {...state}
      new_state.user.styleCodes.push(action.styleCode)
      return new_state
    }

    case UPDATE_STYLE_CODE:
    {
      let new_state = {...state}
      new_state.user.styleCodes =  new_state.user.styleCodes.map(item=>{
        if(item._id == action.styleCode._id){
          return action.styleCode
        }
        return item;
      })
      return new_state
    }
    case DELETE_STYLE_CODE:
    {
      let new_state = {...state}
      new_state.user.styleCodes = new_state.user.styleCodes.filter(item=>item._id != action.id)
      return new_state
    }

    default: return state;
  }
}
