import axios from 'axios';

import {DELETE_STYLE_CODE} from './types';
const API_URL = process.env.API_URL;

export function userSignupRequest(userData) {

    return axios({
        method: 'post',
        url: API_URL + '/user/register',
        data: userData
    })

}
export function addStyleCode(data) {

    return axios({
        method: 'post',
        url: API_URL + '/user/addStyleCode',
        data
    })

}

export function updateListStyleCodesInRedux(type, styleCode) {
    return {
        type,
        styleCode
    };
}


export function updateStyleCode(id, data) {

    return axios({
        method: 'put',
        url: API_URL + '/user/updateStyleCode/' + id,
        data
    })

}

export function deleteStyleCode(id) {


    return dispatch=> {

        axios({
            method: 'put',
            url: API_URL + '/user/deleteStyleCode',
            data: {id}
        }).then(res=> {
            dispatch({
                type: DELETE_STYLE_CODE,
                id
            })

        })


    }
}

export function getStyleCodes() {

    return axios({
        method: 'get',
        url: API_URL + '/user/getAllStyleCodes'
    })

}
export function searchStyleCodes(keyword) {

    return axios({
        method: 'get',
        url: API_URL + '/user/searchStyleCodes?keyword=' +keyword
    })

}
export function updateProfile(data) {

    const config = {
        headers: {'content-type': 'multipart/form-data'}
    }

    return axios.post(API_URL + '/user/updateProfile', data, config)
}
export function changePassword(data) {
    return axios.put(API_URL + '/user/changePassword', data)
}
