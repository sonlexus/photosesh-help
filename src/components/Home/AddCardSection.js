import React, {Component} from 'react';
import {Route,Link} from 'react-router-dom'
import {Row, Form,Alert} from 'antd';
import {connect} from 'react-redux'
import { Layout, Menu, Icon,Table } from 'antd';
import axios from 'axios';
import braintree from 'braintree-web'
import AddCardForm from './Include/AddCardForm'
import {getPaymentToken,getCardsHelp} from '../../actions/paymentActions'
const {  Content, Sider } = Layout;
const FormItem = Form.Item;

class AddCardSection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listCards:[]
        }
    }
    componentWillMount(){

        getCardsHelp().then(({data})=>{


            this.setState({listCards:data.data})
        })

        getPaymentToken()
            .then(res=>{
                this.setState({
                    nounce:res.data.data
                })
            })

    }


    render() {

        const dataSource =  this.state.listCards.map(item=>{
            return {

                key:item._id,
                status:item.status,
                number:item.cardNumber,
                createdAt:item.createdAt
            }
        })
        const columns = [{
            title: 'card number',
            dataIndex: 'number',
            key: 'number',
        }, {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
        }, {
            title: 'Created at',
            dataIndex: 'createdAt',
            key: 'createdAt',
        }];




        return (
            <div>
                <AddCardForm {...this.props} token={this.state.nounce}></AddCardForm>
                <Table dataSource={dataSource} columns={columns} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user:state.auth.user
    }
}
export default connect(mapStateToProps, {})(AddCardSection)



