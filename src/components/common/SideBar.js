import style from './sidebar.css'
import React, {Component} from 'react';
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import {logout} from '../../actions/authActions'
import {Popconfirm,Badge,Icon,Layout,Modal} from 'antd'
import {isMobile} from 'react-device-detect'
import classnames from 'classnames'
import {getMobileOperatingSystem} from '../../utils/helper'
const {Sider} = Layout;

const DOMAIN_NAME = process.env.DOMAIN_NAME



function sharetwitter(url,message) {
    return 'https://twitter.com/intent/tweet?text=' + encodeURIComponent(url + "  " +  message);
}



class Sidebar extends Component {
    state = {
        collapsed: false,
        showSidebar: true
    }
    onCollapse = (collapsed) => {
        this.setState({ collapsed });
    }
    toggleSidebar = () => {

        if(isMobile){
            this.setState(state=>{
                return { showSidebar : !state.showSidebar };
            });
        }


    }


    goTo(route){
        this.props.history.replace(route)
    }

    componentWillMount(){
        if(isMobile){
            this.setState({ collapsed:true,showSidebar:false });
        }
    }



    render() {
        let shareViaText = "sms:"+ ((getMobileOperatingSystem() == 'iOS' ) ? '?&' : '?') + "body=" + encodeURIComponent(this.props.user.shareText.replace('refCode',this.props.user.referralCode) + " " + this.props.user.shareURL  )
         let CommonMenu = ()=>{
            return (
                <ul>
                    <li><Link to={'/activity'}>Activity <Icon type="facebook" /></Link></li>
                    <li><Link to={'/bookings'}>Bookings <span className={style.badge}>{(this.props.total_bookings)?this.props.total_bookings:''}</span></Link></li>
                    <li><Link className={style.btn_grey} to={'/proposals'}>Proposals</Link></li>
                    <li><Link to={'/settings'}>Settings</Link></li>
                    <li>
                        <a onClick={()=>{
                            Modal.info({
                                iconType:'info-circle',
                                title: '',
                                content: (<div>Please check out the <a target="_blank" href="https://photosesh.freshdesk.com/support/solutions/folders/19000055989">PhotoSesh Help Center</a> for the quick solution or contact us by email at
                                    <a target="_blank" href="mailto:support@photosesh.com"> Support@photosesh.com</a></div>),
                            });
                        }}>Support</a>
                    </li>
                    <li>
                        <a onClick={()=>{
                            Modal.info({
                                iconType:'',
                                title: '',
                                okText:'Close',
                                content: (<div style={{fontSize: 20}}>Share your refferal code : <strong>{this.props.user.referralCode}</strong>
                                    <p>share on : </p>
                                    <div className={style.share_list}>
                                        {isMobile && <a target="_blank" href={shareViaText}><span className={style.share_via_text}></span></a>}
                                        <a target="_blank" href={fbLinkShare}><span className={style.icon_fb}></span></a>
                                        <a target="_blank" href={twitterLinkShare}><span className={style.icon_twitter}></span></a>
                                        <a target="_blank" href={`mailto:?subject=photosesh now&body=${encodeURIComponent(this.props.user.shareText.replace('refCode',this.props.user.referralCode) + " " + this.props.user.shareURL  )}`}><span className={style.icon_email}></span></a>
                                    </div>

                                </div>),
                            });
                        }}>Earn Credits</a>
                    </li>
                    <li className={style.devider}></li>
                    <li><Link className={style.btn_transparent} to={'/book'}>New PhotoSesh</Link></li>
                    <li style={{marginTop: 40}}>
                        <Popconfirm placement="top" title={'Are you sure logout ?'} onConfirm={()=>{
                            this.props.logout();
                            this.goTo('/login')
                        }} okText="Yes" cancelText="No">
                            <a>LOGOUT</a>
                        </Popconfirm>
                    </li>
                </ul>
            )
        }
        let SettingMenu = ()=>(<ul>
            <li><Link onClick={this.toggleSidebar} to={'/settings/profile'}>Edit Profile</Link></li>
            <li><Link onClick={this.toggleSidebar} to={'/settings/payment-credits'}>Payment</Link></li>
            <li><Link onClick={this.toggleSidebar} to={'/settings/change-password'}>Change Password</Link></li>
            <li><Link onClick={this.toggleSidebar} to={'/settings/style-code'}>PhotoSesh Style Code</Link></li>
        </ul>)

        let {showSidebar} = this.state
        let fbLinkShare = `https://www.facebook.com/sharer/sharer.php?u=${this.props.user.shareURL}`,
            twitterLinkShare = sharetwitter(this.props.user.shareURL,this.props.user.shareText.replace('refCode',this.props.user.referralCode));


        return (
            <div>
                {showSidebar && (
                    <Sider width={(isMobile) ? window.innerWidth : 300} className={classnames(!isMobile ? style.sidebar : style.sidebar_mobile)}
                           collapsible
                           collapsed={this.state.collapsed}
                           onCollapse={this.onCollapse}
                    >
                        <div className={classnames(!isMobile ? style.sidebar : style.sidebar_mobile)}>
                            <div  className={style.logo}>
                                <Link to="/"><img src="/images/PhotoSesh_Logo.jpg" alt=""/></Link>
                            </div>

                            <div className={style.menu}>

                                {!this.state.collapsed && this.props.match.url != '/settings' && (
                                    <CommonMenu />
                                )}

                                {!this.state.collapsed && this.props.match.url == '/settings' && (
                                    <SettingMenu />
                                )}

                                {this.state.collapsed && this.props.match.url != '/settings'  && (
                                    <ul>
                                        <li><Link to={'/activity'}><Icon type="bell" /></Link></li>
                                        <li>
                                            <Link to={'/bookings'}>
                                                <Badge count={this.props.total_bookings}>
                                                    <Icon type="schedule" />
                                                </Badge>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to={'/proposals'}>
                                            </Link>
                                        </li>
                                        <li><Link to={'/settings'}><Icon type="setting" /></Link></li>
                                        <li><Link to={'/support'}><Icon type="customer-service" /></Link></li>

                                        <li>
                                            <a onClick={()=>{
                                                Modal.info({
                                                    iconType:'',
                                                    title: '',
                                                    okText:'Close',

                                                    content: (<div style={{fontSize: 20}}>Share your refferal code : <strong>{this.props.user.referralCode}</strong>
                                                        <p>share on : </p>
                                                        <div className={style.share_list}>
                                                            <a target="_blank" href={shareViaText}><span className={style.share_via_text}></span></a>
                                                            <a target="_blank" href={fbLinkShare}><span className={style.icon_fb}></span></a>
                                                            <a target="_blank" href={twitterLinkShare}><span className={style.icon_twitter}></span></a>
                                                            <a target="_blank" href={`mailto:?subject=photosesh now&body=${encodeURIComponent(this.props.user.shareText.replace('refCode',this.props.user.referralCode) + " " + this.props.user.shareURL  )}`}><span className={style.icon_email}></span></a>
                                                        </div>

                                                    </div>),
                                                });
                                            }}><Icon type="share-alt" /></a>
                                        </li>


                                        <li className={style.devider}></li>
                                        <li><Link  to={'/book'}><Icon type="plus-square-o" /></Link></li>

                                        <li style={{marginTop: 40}}>
                                            <Popconfirm placement="top" title={'Are you sure logout ?'} onConfirm={()=>{
                                                this.props.logout();
                                                this.goTo('/login')
                                            }} okText="Yes" cancelText="No">
                                                <a href=""><Icon style={{cursor:'pointer'}} type="logout" /></a>
                                            </Popconfirm>
                                        </li>

                                        <li><span onClick={this.toggleSidebar} className={style.btn_hide_sidebar}><Icon type="left" /></span></li>

                                    </ul>
                                )}

                                {this.state.collapsed && this.props.match.url == '/settings' && (
                                    <ul>
                                        <li><Link to={'/settings/profile'}><Icon type="setting" /></Link></li>
                                        <li><Link to={'/settings/payment-credits'}><Icon type="wallet" /></Link></li>
                                        <li><Link to={'/settings/change-password'}><Icon type="qrcode" /></Link></li>
                                        <li><Link to={'/settings/style-code'}><Icon type="disconnect" /></Link></li>
                                        <li><Link to={'/'}><Icon type="home" /></Link></li>
                                        <li><span onClick={this.toggleSidebar} className={style.btn_hide_sidebar}><Icon type="left" /></span></li>
                                    </ul>
                                )}

                            </div>
                        </div>



                    </Sider>
                ) }


                {!showSidebar && (

                    <div className={style.sidebar_bottom_wrap}  onClick={this.toggleSidebar}>
                        <span className={style.btn_show_sidebar}><Icon type="menu-unfold" /></span>
                    </div>

                )}

            </div>
        );
    }
}


const mapStateToProps = (state)=>{
    return {
        total_bookings: state.projects.bookings.filter(item=>['ACCEPTED','ACTIVE','COMPLETED'].indexOf(item.appointmentStatus) != -1).length,
        user:state.auth.user
    }
}
export default connect(mapStateToProps,{logout})(Sidebar)
