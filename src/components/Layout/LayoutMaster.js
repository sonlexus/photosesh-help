import style from './layoutmaster.css'
import React from 'react'
import {Layout} from 'antd'

import classnames from 'classnames'
const { Content,Sider} = Layout;


const LayoutMaster = ({ children }) => (

    <Layout className={classnames(style.layout_master,'ant-layout-has-sider')}>
        <Layout>
            <Content>
                <div style={{maxWidth:960,margin:'auto'}}>
                    {children}
                </div>
            </Content>
        </Layout>
    </Layout>
);
export default LayoutMaster

