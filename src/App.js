import React, {Component} from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';
import LayoutMaster from './components/Layout/LayoutMaster'

import DashboardWrapper from './components/Home/AddCardSection'
import { Alert } from 'antd';

import ReloadAnimate from './components/common/Include/ReloadAnimate';



class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            reload:false
        }
        this.props.history.listen((location, action) => {
            this.setState({reload:true})
            setTimeout(()=>{ this.setState({reload:false}) },700)
        });
    }


    generateLayout = (props, Layout, Component) => (<Layout {...props}><Component  {...props}  /></Layout>)


    render() {
        return (
            <div>
                {this.state.reload && (<ReloadAnimate/>)}

                <Switch>
                    <Route path="/" render={(props)=> this.generateLayout(props, LayoutMaster, DashboardWrapper)}/>
                    <Route path="/add-card" render={(props)=> this.generateLayout(props, LayoutMaster, DashboardWrapper)}/>
                    <Redirect to="/"/>
                </Switch>
            </div>

        );
    }
}

export default App;
