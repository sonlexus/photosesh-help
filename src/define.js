import moment from 'moment';

export const DURATIONS = ['0.5', '1', '1.5', '2', '2.5', '3', '3.5', '4', '4.5', '5', '5.5', '6']
export const TIME = ['12:00 AM', '12:30 AM', '01:00 AM', '01:30 AM', '02:00 AM', '02:30 AM', '03:00 AM', '03:30 AM', '04:00 AM', '04:30 AM', '05:00 AM', '05:30 AM', '06:00 AM', '06:30 AM', '07:00 AM', '07:30 AM', '08:00 AM', '08:30 AM', '09:00 AM', '09:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '01:00 PM', '01:30 PM', '02:00 PM', '02:30 PM', '03:00 PM', '03:30 PM', '04:00 PM', '04:30 PM', '05:00 PM', '05:30 PM', '06:00 PM', '06:30 PM', '07:00 PM', '07:30 PM', '08:00 PM', '08:30 PM', '09:00 PM', '09:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', '11:30 PM']
export const PREPARE_TIME = 15
export const NOW = 'now'
export const LATER = 'later'
export const GRID = 'GRID'
export const LIST = 'LIST'
export const MILE = 0.621371192
export const LOCAL_STORAGE_EXPIRE = 60*60*6
export const NO_FOUND_PHOTOGRAPHERS = 'Photographers within your criteria are currently busy. Please consider changing your date/time. Or Category. We appreciate your patience as we are growing quickly.'
export const UTC_OFFSET = moment(new Date()).utcOffset()
export const STATUS = {
    ALL:'ALL',
    PENDING:'PENDING',
    COMPLETED:'COMPLETED',
    ACTIVE:'ACTIVE',
    EXPIRED:'EXPIRED',
    CANCELLED:'CANCELLED',
    ACCEPTED:'ACCEPTED',
    REJECTED:'REJECTED',
    CHANGE_REJECTED:'CHANGE_REJECTED',
}


export const EVENT_TYPE = {
    AGENT_ACCEPT_APPOINTMENT : 'AGENT_ACCEPT_APPOINTMENT',
    AGENT_DECLINE_APPOINTMENT : 'AGENT_DECLINE_APPOINTMENT',
    AGENT_APPROVE_EXTRATIME : 'AGENT_APPROVE_EXTRATIME',
}

